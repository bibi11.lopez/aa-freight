From d457f998c13c7909173738aeed86adaa93d9e6c4 Mon Sep 17 00:00:00 2001
From: Rebecca Murphy <rebecca@rcmurphy.me>
Date: Tue, 6 Oct 2020 00:32:38 +0000
Subject: [PATCH] Add break above inner top navigation.

---
 freight/templates/freight/base.html | 1 +
 1 file changed, 1 insertion(+)

diff --git a/freight/templates/freight/base.html b/freight/templates/freight/base.html
index 3e62817..0fff0d0 100644
--- a/freight/templates/freight/base.html
+++ b/freight/templates/freight/base.html
@@ -4,6 +4,7 @@
 
 {% block content %}
     <div class="col-lg-12">
+        <br />
         {% include 'freight/menu.html' %}
         <h2 class="text-center">{{ page_title }}</h2>
         <div class="col-lg-12 container">            
-- 
GitLab

